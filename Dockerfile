FROM ubuntu:20.04
LABEL maintainer="denaumenko2@gmail.com"
ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Kyiv
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && apt-get update -y && \
    apt-get upgrade -y && apt-get install zabbix-server-pgsql zabbix-frontend-php -y && \ 
    apt-get -y install postgresql-12 postgresql-client-12 
COPY . ./app
RUN cp /app/pg_hba.conf /etc/postgresql/12/main/ &&  \ 
    cp /app/zabbix_server.conf /etc/zabbix/ && \
    cp /app/php.ini etc/php/7.4/apache2/php.ini
USER postgres
RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER zabbix WITH SUPERUSER PASSWORD 'zabbix';" &&\
    createdb -O zabbix -E Unicode -T template0 zabbix 
EXPOSE 5432
USER root
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf && \
    ln -s /usr/share/zabbix /var/www/html 
EXPOSE 80
CMD pg_ctlcluster 12 main start
USER zabbix
CMD zcat /usr/share/zabbix-server-pgsql*/schema.sql.gz | psql zabbix
USER root
CMD service apache2 start && \
    service zabbix-server start
VOLUME  ["/etc/postgresql", "/var/log/postgresql", "/var/lib/postgresql"]
CMD ["apachectl", "-D", "FOREGROUND"] && \
    ["/bin/bash", "-c", "service postgresql start && service postgresql status && sleep infinity"]




